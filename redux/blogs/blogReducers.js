import {
  CREATE_POST,
  FAILED_POST,
  GET_ALL_POST,
  GET_MY_POST,
  SEARCH_POST,
  EDIT_POST,
  REMOVE_POST,
} from "./blogTypes";

const initialState = {
  create: "",
  posts: [],
  errorMessage: "",
};

export const postReducers = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_POST:
      return { ...state, create: action.payload };
    case GET_ALL_POST:
      return { ...state, posts: action.payload.data };
    case FAILED_POST:
      return { ...state, errorMessage: action.payload };
    default:
      return state;
  }
};
