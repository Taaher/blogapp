import {
  CREATE_POST,
  GET_ALL_POST,
  GET_MY_POST,
  SEARCH_POST,
  EDIT_POST,
  REMOVE_POST,
  FAILED_POST,
} from "./blogTypes";
import axios from "axios";

export const createPost = (formProps) => async (dispatch) => {
  console.log(formProps);
  try {
    const url = "/api/v1/";
    const token = process.browser ? localStorage.getItem("token") : null;
    console.log("set token in the post blog", token);
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    const response = await axios.post(url, formProps, config);

    dispatch({ type: CREATE_POST, payload: response.data });
  } catch (error) {
    dispatch({ type: FAILED_POST, payload: error });
    console.log(error);
  }
};

export const getAllPost = async (dispatch) => {
  try {
    const response = await axios.get("/blog");
    console.log(response.data.data);
    dispatch({ type: GET_ALL_POST, payload: response.data });
  } catch (error) {
    dispatch({ type: FAILED_POST, payload: error });
    console.log(error);
  }
};
