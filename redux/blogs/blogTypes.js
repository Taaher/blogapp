export const CREATE_POST = "CREATE_POST";
export const GET_ALL_POST = "GET_ALL_POST";
export const GET_MY_POST = "GET_MY_POST";
export const SEARCH_POST = "SEARCH_POST";
export const EDIT_POST = "EDIT_POST";
export const REMOVE_POST = "REMOVE_POST";
export const FAILED_POST = "FAILED_POST";
