import { AUTH_USER, AUTH_ERROR, SIGNOUT_USER } from "./authTypes";

const initialState = {
  authenticated: "",
  errorMessage: "",
};
export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_USER:
      return { ...state, authenticated: action.payload };
    case AUTH_ERROR:
      return { ...state, errorMessage: action.payload };
    case SIGNOUT_USER:
      //can removed action . payload == empty string
      return { ...state, authenticated: "", errorMessage: "" };
    default:
      return state;
  }
};
