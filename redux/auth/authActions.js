import { AUTH_USER, AUTH_ERROR, SIGNOUT_USER } from "./authTypes";
import axios from "axios";
import Router from "next/router";

/*






*/
export const signupUser = (formProps) => async (dispatch) => {
  try {
    const response = await axios.post("/account/signup", formProps);

    dispatch({ type: AUTH_USER, payload: response.data.token });
    Router.push("/");
  } catch (error) {
    dispatch({ type: AUTH_ERROR, payload: error });
    console.log(error);
  }
};
/*




*/
export const signinUser = (formProps) => async (dispatch) => {
  try {
    const response = await axios.post("/signin", formProps);
    let token = response.data.token;
    localStorage.setItem("token", token);

    dispatch({ type: AUTH_USER, payload: response.data.token });
    Router.push("/");
  } catch (error) {
    dispatch({ type: AUTH_ERROR, payload: error });
  }
};
/* 




*/
export const signOutUser = (dispatch) => {
  console.log("clicked siguout");
  try {
    localStorage.removeItem("token");
    dispatch({ type: SIGNOUT_USER, payload: "" });

    Router.push("/");
  } catch (error) {
    dispatch({ type: AUTH_ERROR, payload: error });
  }
};
