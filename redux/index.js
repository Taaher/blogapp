import { combineReducers } from "redux";
import { authReducer } from "./auth/AuthReducers";
import { postReducers } from "./blogs/blogReducers";
import { reducer as formReducer } from "redux-form";
// import { persistReducer } from "redux-persist";
// import storage from "redux-persist/lib/storage"; // defaults to localStorage for web

const rootReducer = combineReducers({
  blog: postReducers,
  auth: authReducer,
  form: formReducer,
});

export default rootReducer;

// const rootPersistConfig = {
//   key: "root",
//   storage: storage,
//   blacklist: ["auth"],
// };

// const authPersistConfig = {
//   key: "token",
//   storage: storage,
// };

// const rootReducer = combineReducers({
//   auth: persistReducer(authPersistConfig, authReducer),
//   blog: postReducers,
//   form: formReducer,
// });

// export default persistReducer(rootPersistConfig, rootReducer);
