import React from "react";
import Link from "next/link";
import { useDispatch, useSelector } from "react-redux";
import { signOutUser } from "./../redux/auth/authActions";

export default function Header() {
  const auth = useSelector((state) => state.auth.authenticated);
  const dispatch = useDispatch();

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-color">
        <div className="container">
          <a className="navbar-brand" href="/">
            <img
              className="img-logo"
              src="/blueTz.svg"
              alt="tz-logo"
              height="40px"
              width="50px"
            />
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link href="/">
                  <a className="nav-link">Home</a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/blog">
                  <a className="nav-link">Blog</a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/about">
                  <a className="nav-link">about</a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/contact">
                  <a className="nav-link">contact</a>
                </Link>
              </li>
              {auth ? (
                <li className="nav-item">
                  <Link href="/">
                    <button
                      className="nav-link btn btn-outline-primary mr-2"
                      onClick={() => dispatch(signOutUser)}
                    >
                      Exits
                    </button>
                  </Link>
                </li>
              ) : (
                <main className="row ml-2">
                  <li className="nav-item">
                    <Link href="/account/signup">
                      <a className="nav-link btn btn-outline-primary mr-2">
                        SignUp
                      </a>
                    </Link>
                  </li>

                  <li className="nav-item">
                    <Link href="/account/signin">
                      <a className="nav-link btn btn-primary">Signin</a>
                    </Link>
                  </li>
                </main>
              )}
            </ul>
          </div>
        </div>
      </nav>
      <style jsx>{`
        .bg-color {
          background-color: rgb(2, 2, 65);
        }
        .nav-link {
          color: white !important;
        }
        .nav-link:hover {
          color: grey !important;
        }
      `}</style>
    </div>
  );
}
