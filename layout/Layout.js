import Header from "./Header";
import Footer from "./Footer";
import Router from "next/router";
import NProgress from "nprogress";
import Head from "next/head";

Router.onRouteChangeStart = () => NProgress.start();
Router.onRouteComplete = () => NProgress.done();
Router.onRouteChangeError = () => NProgress.done();

export default function Layout({children}) {
  return (
    <div>
      <Head>
        <title>App-Tz</title>
      </Head>
      <Header />
      {children}
      <Footer />
    </div>
  );
}
