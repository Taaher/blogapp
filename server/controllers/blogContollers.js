const Blog = require("../models/blog");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middlewares/asyncHandler");

/*
 METHOD : 'POST'
 JUST ADMIN CAN BE CREATED PUBLISH
 AUTHOR == DRAFT POST
*/

exports.createPost = asyncHandler(async (req, res, next) => {
  let blog = await Blog.create(req.body);

  //Checked if : admin published post else : draft post
  adminPublished(blog);

  res.status(201).json({
    success: true,
    data: blog,
  });
});

/*
MEHTOD : 'GET'
'JUST GET POST PUBLISHED'
*/

exports.getPosts = asyncHandler(async (req, res, next) => {
  const blogs = await Blog.find({ published: "publish" });
  res.status(200).json({
    success: true,
    data: blogs,
  });
});
/*
METHOD : 'GET'
DES : 'GET BY ID'
IF PUBLISHED CAN SHOW ELSE : JUST ADMIN CAN B CHANGE OR SEE 
*/
exports.getPost = asyncHandler(async (req, res, next) => {
  const blog = await Blog.findById(req.params.id);
  if (!blog) {
    return next(new ErrorResponse(`this id not availabe!`, 404));
  }
  if (blog.published == "draft") {
    return next(new ErrorResponse(`u can not see that!`, 404));
  }
  res.status(200).json({
    success: true,
    data: blog,
  });
});
exports.updatePost = asyncHandler(async (req, res, next) => {
  const blog = await Blog.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });
  //checked!
  if (!blog) {
    return next(new ErrorResponse(`this id not availabe!`, 404));
  }
  //checked!
  if (req.user.role === "author") {
    blog.published = "draft";
  } else if (req.user.role === "admin") {
    blog.published = "publish";
  }

  blog.save();
  res.status(200).json({
    success: true,
    data: blog,
  });
});
exports.deletePost = asyncHandler(async (req, res, next) => {
  const blog = await Blog.findByIdAndRemove(req.params.id);
  if (!blog) {
    return next(new ErrorResponse(`this id not availabe!`, 404));
  }
  res.status(200).json({
    success: true,
    data: "removed",
  });
});

const adminPublished = (post) => {
  if (req.user.role === "admin") {
    blog.published = "publish";
    blog.save();
  }
};
