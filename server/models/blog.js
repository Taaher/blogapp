const mongoose = require("mongoose");

const BlogSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  body: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  tags: [String],
  updated: {
    type: Date,
  },
  published: {
    type: String,
    enum: ["draft", "publish"],
    default: "draft",
  },
});

module.exports = mongoose.model("Blog", BlogSchema);
