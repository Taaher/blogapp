require("dotenv").config({ path: "./config/config.env" });
const express = require("express");
const morgan = require("morgan");
const helmet = require("helmet");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const blogRouter = require("./routes/blogRouter");
const userRouter = require("./routes/userRouter");
const errorHandler = require("./middlewares/errorHandler");
const connectDB = require("./config/db");
const next = require("next");
const { parse } = require("url");
//connection to Database
connectDB();
//
//config application
const PORT = process.env.PORT || 3000;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

//configuration server and port

app.prepare().then(() => {
  const server = express();

  //config bodyParser
  server.use(bodyParser.json());
  //config morgan

  server.use(morgan("combined"));

  //config helmet
  server.use(helmet());

  //config cookie parser
  server.use(cookieParser());

  //config cors
  server.use(cors());

  //configuration routers
  server.use("/blog", blogRouter);
  server.use("/account", userRouter);

  //errorHandler
  server.use(errorHandler);

  if (process.env.NODE_ENV === "development") {
    app.use(morgan("dev"));
  }

  //new handle
  server.get("*", (req, res) => {
    return handle(req, res);
  });

  server.listen(PORT, () => console.log(`server is started on port ${PORT}`));
});
