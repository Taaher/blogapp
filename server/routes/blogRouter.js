const express = require("express");
const router = express.Router();
const { protect, authorize } = require("../middlewares/auth");
const {
  createPost,
  getPosts,
  getPost,
  updatePost,
  deletePost,
} = require("../controllers/blogContollers");

//authorize(protect,"author", "admin")
//authorize('author','admin')
router.post("/",protect,authorize('author','admin'),createPost);
router.get("/", getPosts);
router.get("/:id", getPost);
router.put("/:id", protect, authorize("admin", "author"), updatePost);
router.delete("/:id", protect, authorize("admin"), deletePost);
module.exports = router;

