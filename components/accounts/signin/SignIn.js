import React, { useEffect, useState } from "react";
import { reduxForm, Field } from "redux-form";
import { useSelector, useDispatch } from "react-redux";
import { signinUser } from "../../../redux/auth/authActions";
import { FaEye } from "react-icons/fa";
import { useFormik } from "formik";
import * as Yup from "yup";


function SignIn(props) {
  const errorMessage = useSelector((state) => state.auth.errorMessage);
  console.log(errorMessage);
  const dispatch = useDispatch();
  useEffect(() => {
    const errorMessage = "";
    return () => errorMessage;
  }, [errorMessage]);
  const onSubmit = (formProps) => {
    console.log(formProps);
    dispatch(signinUser(formProps));
  };

  const { handleSubmit } = props;

  const [eyePassword, eyePasswordSet] = useState("password");

  // eyePassword
  const handleChangeEye = () => {
    if (eyePassword === "password") {
      eyePasswordSet("text");
    } else {
      eyePasswordSet("password");
    }
  };

  return (
    <div className="container cont-signup">
      <div className="row">
        <div className="col-xl-5 col-lg-6 col-md-8 col-sm-10 mx-auto form p-4  sign-up">
          <h2 align-text="center"> Please Sign in Account ! </h2>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="form-group">
              <label htmlFor="Email">Email:</label>
              <fieldset>
                <Field
                  type="text"
                  name="email"
                  component="input"
                  placeholder="Example@email.com"
                  className="form-control"
                  autoComplete="none"
                  required={true}
                />
              </fieldset>
            </div>

            <div className="input-group mb-3">
              <fieldset>
                <Field
                  type={eyePassword}
                  name="password"
                  component="input"
                  placeholder="Password"
                  className="form-control"
                  autoComplete="none"
                  required={true}
                />
                <div className="input-group-append">
                  <button
                    className="btn btn-outline-secondary"
                    type="button"
                    onClick={handleChangeEye}
                  >
                    <FaEye />
                  </button>
                </div>
              </fieldset>
            </div>

            <div className="form-check">
              <fieldset>
                <Field
                  input={{ defaultChecked: false }}
                  component="input"
                  type="checkbox"
                  className="form-check-input"
                  name="remmberme"
                />{" "}
                Remmber me
              </fieldset>
            </div>

            <button type="submit" className="btn btn-outline-primary mt-2">
              Sign in Account
            </button>

            {errorMessage ? (
              <div className="alert alert-danger mt-3" role="alert">
                Error please check your email or password
              </div>
            ) : null}
          </form>
        </div>
      </div>
    </div>
  );
}

export default reduxForm({ form: "signin" })(SignIn);
