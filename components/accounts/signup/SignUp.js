import React from "react";

import { reduxForm, Field } from "redux-form";
import { signupUser } from "../../../redux/auth/authActions";
import { useDispatch, useSelector } from "react-redux";

const SignUp = (props) => {
  const errorMessage = useSelector((state) => state.auth.errorMessage);
  const dispatch = useDispatch();

  const onSubmit = (formProps) => {
    console.log(formProps);
    dispatch(signupUser(formProps));
  };


  const { handleSubmit } = props;
  return (
    <div className="cont-signup container">
      <div className="row">
        <div className="col-xl-5 col-lg-6 col-md-8 col-sm-10 mx-auto form p-4  sign-up">
          <h2 align-text="center"> Create Account ! </h2>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="form-group">
              <label htmlFor="username">Username</label>
              <fieldset>
                <Field
                  type="text"
                  name="username"
                  component="input"
                  placeholder="Username"
                  className="form-control"
                  autoComplete="none"
                  required="true"
                />
              </fieldset>
            </div>
            <div className="form-group">
              <label htmlFor="Email">Email:</label>
              <fieldset>
                <Field
                  type="text"
                  name="email"
                  component="input"
                  placeholder="Email@example.com"
                  className="form-control"
                  autoComplete="none"
                  required="true"
                />
              </fieldset>
            </div>

            <div className="form-group">
              <label htmlFor="password">Password:</label>
              <fieldset>
                <Field
                  type="password"
                  name="password"
                  component="input"
                  placeholder="Password"
                  className="form-control"
                  autoComplete="none"
                  required="true"
                />
              </fieldset>
            </div>
            <button type="submit" className="btn btn-outline-primary">
              Sign up Account
            </button>
            {errorMessage ? (
              <div class="alert alert-danger mt-5" role="alert">
                <strong>
                  please Check your name and email ,password length 6 character!
                </strong>{" "}
                <a href="#" class="alert-link"></a>
              </div>
            ) : null}
          </form>
        </div>
      </div>
    </div>
  );
};

export default reduxForm({ form: "signup" })(SignUp);
