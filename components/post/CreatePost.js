import React from "react";
import { Field, reduxForm } from "redux-form";
import { createPost } from "../../redux/blogs/blogActions";
import { useDispatch, useSelector } from "react-redux";
//title //body //date//tags//published
const CreatePost = (props) => {
  const erorrMesssage = useSelector((state) => state.blog.errorMessage);
  const dispatch = useDispatch();
  const onSubmit = (formProps) => {
    console.log(formProps);
    dispatch(createPost(formProps));
  };
  const { handleSubmit } = props;
  return (
    <div className="create_post">
      <div className="container">
        <div className="row">
          <div className="col-xl-5 col-lg-6 col-md-8 col-sm-10 col-xs-4 mx-auto form p-4">
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="form-group">
                <label htmlFor="">title : </label>
                <fieldset>
                  <Field
                    type="text"
                    name="title"
                    component="input"
                    className="form-control"
                    placeholder="type title"
                    autoComplete="none"
                    required={true}
                  />
                </fieldset>
              </div>
              <div className="form-group">
                <label htmlFor="">body : </label>
                <fieldset>
                  <Field
                    type="text"
                    name="body"
                    component="input"
                    className="form-control"
                    placeholder="type body"
                    autoComplete="none"
                    required={true}
                  />
                </fieldset>
              </div>
              <div className="form-group">
                <label htmlFor="">tags : </label>
                <fieldset>
                  <Field
                    type="text"
                    name="tags"
                    component="input"
                    className="form-control"
                    placeholder="type tags"
                    autoComplete="none"
                    required={true}
                  />
                </fieldset>
              </div>
              <button type="submit" className="btn btn-outline-primary">
                Create Post
              </button>
              {erorrMesssage ? (
                <div class="alert alert-danger" role="alert">
                  you nothing create account Firest signin
                </div>
              ) : null}
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default reduxForm({ form: "blog" })(CreatePost);
