import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getAllPost } from "./../../redux/blogs/blogActions";

const GetAllPost = () => {
  const posts = useSelector((state) => state.blog.posts);
  const dispatch = useDispatch();
  //

  useEffect(() => {
    console.log("Get all post started");
    dispatch(getAllPost);
  }, []);

  return (
    <div>
      <div className="container text-center alert alert-primary" role="alert">
        all Blog post published u Can Send new Post
      </div>
      {posts.map((post) => (
        <div className="card text-dark container mt-3" key={post._id}>
          <div className="card-body">
            <h4 className="card-title">{post.title}</h4>
            <p className="card-text">{post.body}</p>
          </div>
        </div>
      ))}
    </div>
  );
};

export default GetAllPost;
