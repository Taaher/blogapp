import React from "react";

export default function Cover() {
  return (
    <div>
      <div className="inner cover text-center  p-5 ">
        <h1 className="cover-heading mt-5">Live with code .</h1>
        <p className="lead">
          Cover is a one-page template for building simple and beautiful home
          pages. Download, edit the text, and add your own fullscreen background
          photo to make it your own.
        </p>
        <p className="lead">
          <a href="#" className="btn btn-lg btn-outline-primary">
            Learn more
          </a>
        </p>
      </div>
      <style>{`
      .inner {
          background-color :   rgb(31, 0, 116);
          height : 400px;
      }
      .cover-heading {
        color : white;
      }
      .lead {
        color : white;
      }
      `}</style>
    </div>
  );
}
