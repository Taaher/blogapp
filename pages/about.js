import { useSelector } from "react-redux";
import Router from "next/router";
import React, { useEffect } from "react";
import Redirect from "nextjs-redirect";

export default function about() {
  const auth = useSelector((state) => state.auth.authenticated);
  console.log(auth);
  useEffect(() => {
    if (auth == null) {
      // Redirect("/");
      Router.push("/");
    }
  }, [auth]);
  return <div>{auth ? "Hllo" : "bye"}</div>;
}
