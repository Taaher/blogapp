import React, { useState } from "react";
import localForage from "localforage";

export default function () {
  const [auth, setAuth] = useState("");
  localForage
    .getItem("token")
    .then(function (value) {
      console.log(value);
      setAuth(value);
    })
    .catch(function (err) {
      console.log(err);
    });
  return auth;
}
