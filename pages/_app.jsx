import Layout from "../layout/Layout";
import "./../layout/style/styles.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import { Provider } from "react-redux";
import rootReducer from "./../redux";
import { createStore, applyMiddleware } from "redux";
import reduxThunk from "redux-thunk";

const store = createStore(
  rootReducer,
  {
    auth: {
      authenticated: process.browser ? localStorage.getItem("token") : null,
    },
  },
  applyMiddleware(reduxThunk)
);

export default function app({ Component, pagePorps }) {
  return (
    <Provider store={store}>
      <Layout>
        <Component {...pagePorps} />
      </Layout>
    </Provider>
  );
}

/*



















*/

// import { PersistGate } from "redux-persist/integration/react";
// import configureStore from "./../store";
// const { store, persistor } = configureStore();
// Callback version:
// localForage.getItem("token", function (err, value) {
//   console.log("my Token is!", value);
// });

// export default function app({ Component, pagePorps }) {
//   return (
//     <Provider store={store}>
//       <Layout>
//         <PersistGate loading={null} persistor={persistor}>
//           <Component {...pagePorps} />
//         </PersistGate>
//       </Layout>
//     </Provider>
//   );
// }
