import GetAllPost from "../components/post/GetAllPost";
import CreatePost from "../components/post/CreatePost";

export default function Blog() {
  return (
    <div>
      <CreatePost />
      <GetAllPost />
    </div>
  );
}
