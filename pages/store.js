import React from "react";
import { useRouter } from "next/router";

function store() {
  const router = useRouter();
  const { id } = router.query;
  console.log(id);
  return <div>checked id : {id}</div>;
}

export default store;
