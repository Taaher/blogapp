import Document, { Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  render() {
    return (
      <html>
        <Head>
          <meta charset="UTF-8" />
          <meta name="description" content="application Taher" />
          <meta name="keywords" content="NextJs,Reactjs" />
          <meta name="author" content="Taher" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}

export default MyDocument;
